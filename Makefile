.PHONY: overview

default: diagrams

bootstrap-workflow-svg: src/openshift-cluster/bootstrap-workflow.puml
	docker run --rm -v "$(shell pwd)"/src/openshift-cluster/bootstrap-workflow.puml:/input.puml -v "$(shell pwd)"/svg/openshift-cluster/bootstrap-workflow:/output steppodrom/plantuml -tsvg /input.puml -o output/ -progress -nbthread auto

bootstrap-workflow-png: src/openshift-cluster/bootstrap-workflow.puml
	docker run --rm -v "$(shell pwd)"/src/openshift-cluster/bootstrap-workflow.puml:/input.puml -v "$(shell pwd)"/png/openshift-cluster/bootstrap-workflow:/output steppodrom/plantuml -tpng /input.puml -o output/ -progress -nbthread auto

kubernetes-components-svg: src/openshift-cluster/overview.puml
	docker run --rm -v "$(shell pwd)"/src/openshift-cluster/overview.puml:/input.puml -v "$(shell pwd)"/svg/openshift-cluster/overview:/output steppodrom/plantuml -tsvg /input.puml -o output/ -progress -nbthread auto

kubernetes-components-png: src/openshift-cluster/kubernetes-components.puml
	docker run --rm -v "$(shell pwd)"/src/openshift-cluster/kubernetes-components.puml:/input.puml -v "$(shell pwd)"/png/openshift-cluster/kubernetes-components:/output steppodrom/plantuml -tpng /input.puml -o output/ -progress -nbthread auto

openshift-bootstrap-svg: src/openshift-cluster/openshift-bootstrap.puml
	docker run --rm -v "$(shell pwd)"/src/openshift-cluster/openshift-bootstrap.puml:/input.puml -v "$(shell pwd)"/svg/openshift-cluster/openshift-bootstrap:/output steppodrom/plantuml -tsvg /input.puml -o output/ -progress -nbthread auto

openshift-bootstrap-png: src/openshift-cluster/openshift-bootstrap.puml
	docker run --rm -v "$(shell pwd)"/src/openshift-cluster/openshift-bootstrap.puml:/input.puml -v "$(shell pwd)"/png/openshift-cluster/openshift-bootstrap:/output steppodrom/plantuml -tpng /input.puml -o output/ -progress -nbthread auto

overview-svg: src/openshift-cluster/overview.puml
	docker run --rm -v "$(shell pwd)"/src/openshift-cluster/overview.puml:/input.puml -v "$(shell pwd)"/svg/openshift-cluster/overview:/output steppodrom/plantuml -tsvg /input.puml -o output/ -progress -nbthread auto

overview-png: src/openshift-cluster/overview.puml
	docker run --rm -v "$(shell pwd)"/src/openshift-cluster/overview.puml:/input.puml -v "$(shell pwd)"/png/openshift-cluster/overview:/output steppodrom/plantuml -tpng /input.puml -o output/ -progress -nbthread auto


png: overview-png openshift-bootstrap-png kubernetes-components-png bootstrap-workflow-png

svg: overview-svg openshift-bootstrap-svg kubernetes-components-svg bootstrap-workflow-svg

diagrams: svg png