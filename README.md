# Infrastructure Diagrams for TMDE

run `make` for rebuilding svg and png from puml

## Openshift Cluster

![](png/openshift-cluster/overview/Platform.png)

## Openshift bootstrap

### Workflow

![](png/openshift-cluster/bootstrap-workflow/Bootstrap-Workflow.png)

### Details

![](png/openshift-cluster/openshift-bootstrap/Openshift-Bootstrap.png)

## Openshift Components

### Master

![](png/openshift-cluster/kubernetes-components/Master.png)

### Etcd

![](png/openshift-cluster/kubernetes-components/Etcd.png)

### Nodes

![](png/openshift-cluster/kubernetes-components/AppNode.png)